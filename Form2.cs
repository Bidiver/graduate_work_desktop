﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();
            form1.Show();
            this.Hide();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {   
            if (checkuser())
            {
                return;
            }
            DataBase class1 = new DataBase();
            try
            {
                string querystring = $"insert into users(name, login, password) values('{textBox1.Text}', '{textBox2.Text}', '{textBox3.Text}')";
                NpgsqlCommand command = new NpgsqlCommand(querystring, class1.GetConnection());
                class1.OpenConnection();

                if (command.ExecuteNonQuery() == 1)
                {
                    MessageBox.Show("Аккаунт успешно создан!", "Успех!");
                }
                else
                {
                    MessageBox.Show("Аккаунт не создан!");
                }
            }
            catch
            {
                MessageBox.Show("Непредвиденная ошибка!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            class1.CloseConnection();
        }
        private Boolean checkuser()
        {
            DataBase class1 = new DataBase();
            NpgsqlDataAdapter adapter = new NpgsqlDataAdapter();
            DataTable table = new DataTable();
            string querystring = $"select name, login, password from users where login = '{textBox1.Text}' and password = '{textBox2.Text}'";

            NpgsqlCommand command = new NpgsqlCommand(querystring, class1.GetConnection());

            adapter.SelectCommand = command;
            adapter.Fill(table);

            if (table.Rows.Count > 0)
            {
                MessageBox.Show("Пользователь уже существует!");
                return true;
            }
            else
            {
                return false;
            }
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}

﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp2;

namespace Дипломная_работа_Лоншаков_2023.Classes
{
    internal class DeleteRow
    {
        DataBase dataBase = new DataBase();
        RefreshDate refreshDate = new RefreshDate();
        public void deleteRow_1(string a, DataGridView b, System.Windows.Forms.ComboBox c)
        {
            DialogResult dialogResult = MessageBox.Show("Вы точно хотите удалить запись?", "Удаление", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                string cmd_text = "DELETE FROM busses";
                cmd_text = "DELETE FROM busses WHERE id = '" + a + "'";

                NpgsqlCommand npgsql = new NpgsqlCommand(cmd_text, dataBase.GetConnection());

                dataBase.OpenConnection();
                npgsql.ExecuteNonQuery();
                dataBase.CloseConnection();

                refreshDate.RefreshDataGrid_1(b);
                refreshDate.RefreshComboBox_1(c);
            }
            else if (dialogResult == DialogResult.No)
            {

            }
        }
        public void deleteRow_2(string a, DataGridView b, System.Windows.Forms.ComboBox c, System.Windows.Forms.ComboBox d)
        {
            DialogResult dialogResult = MessageBox.Show("Вы точно хотите удалить запись?", "Удаление", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                string cmd_text = "DELETE FROM cities";
                
                cmd_text = "DELETE FROM cities WHERE id = '" + a + "'";

                NpgsqlCommand npgsql = new NpgsqlCommand(cmd_text, dataBase.GetConnection());

                dataBase.OpenConnection();
                npgsql.ExecuteNonQuery();
                dataBase.CloseConnection();

                refreshDate.RefreshDataGrid_2(b);
                refreshDate.RefreshComboBox_2(c);
                refreshDate.RefreshComboBox_3(d);
            }
            else if (dialogResult == DialogResult.No)
            {

            }
        }
        public void deleteRow_3(string a, DataGridView b)
        {
            DialogResult dialogResult = MessageBox.Show("Вы точно хотите удалить запись?", "Удаление", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                string cmd_text = "DELETE FROM users";
                cmd_text = "DELETE FROM users WHERE id = '" + a + "'";

                NpgsqlCommand npgsql = new NpgsqlCommand(cmd_text, dataBase.GetConnection());

                dataBase.OpenConnection();
                npgsql.ExecuteNonQuery();
                dataBase.CloseConnection();

                refreshDate.RefreshDataGrid_3(b);
            }
            else if (dialogResult == DialogResult.No)
            {

            }
        }
        public void deleteRow_4(string a, DataGridView b)
        {
            DialogResult dialogResult = MessageBox.Show("Вы точно хотите удалить запись?", "Удаление", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                string cmd_text = "DELETE FROM flights";
                cmd_text = "DELETE FROM flights WHERE id = '" + a + "'";

                NpgsqlCommand npgsql = new NpgsqlCommand(cmd_text, dataBase.GetConnection());

                dataBase.OpenConnection();
                npgsql.ExecuteNonQuery();
                dataBase.CloseConnection();

                refreshDate.RefreshDataGrid_4(b);
            }
            else if (dialogResult == DialogResult.No)
            {

            }
        }
        public void deleteRow_5(string a, DataGridView b)
        {
            DialogResult dialogResult = MessageBox.Show("Вы точно хотите удалить запись?", "Удаление", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                string cmd_text = "DELETE FROM entries";
                cmd_text = "DELETE FROM entries WHERE id = '" + a + "'";

                NpgsqlCommand npgsql = new NpgsqlCommand(cmd_text, dataBase.GetConnection());

                dataBase.OpenConnection();
                npgsql.ExecuteNonQuery();
                dataBase.CloseConnection();

                refreshDate.RefreshDataGrid_5(b);
            }
            else if (dialogResult == DialogResult.No)
            {

            }
        }
    }
}

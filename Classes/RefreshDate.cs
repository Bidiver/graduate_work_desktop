﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp2;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace Дипломная_работа_Лоншаков_2023.Classes
{
    internal class RefreshDate
    {
        DataBase dataBase = new DataBase();
        public void RefreshDataGrid_1(DataGridView dgw)
        {
            dgw.Rows.Clear();
            string querystring = $"select * from busses";

            NpgsqlCommand command = new NpgsqlCommand(querystring, dataBase.GetConnection());
            dataBase.OpenConnection();

            NpgsqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                ReadSingleRow_1(dgw, reader);
            }
            reader.Close();
        }
        public void RefreshDataGrid_2(DataGridView dgw)
        {
            dgw.Rows.Clear();
            string querystring = $"select * from cities";

            NpgsqlCommand command = new NpgsqlCommand(querystring, dataBase.GetConnection());
            dataBase.OpenConnection();

            NpgsqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                ReadSingleRow_2(dgw, reader);
            }
            reader.Close();
        }
        public void RefreshDataGrid_3(DataGridView dgw)
        {
            dgw.Rows.Clear();
            string querystring = $"select * from users";

            NpgsqlCommand command = new NpgsqlCommand(querystring, dataBase.GetConnection());
            dataBase.OpenConnection();

            NpgsqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                ReadSingleRow_3(dgw, reader);
            }
            reader.Close();
        }
        public void RefreshDataGrid_4(DataGridView dgw)
        {
            dgw.Rows.Clear();

            string querystring = $"SELECT flights.id, dep_date, travel_time, dep_cities_id, dest_cities_id, busses_id FROM flights";

            NpgsqlCommand command = new NpgsqlCommand(querystring, dataBase.GetConnection());
            dataBase.OpenConnection();

            NpgsqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                ReadSingleRow_4(dgw, reader);
            }
            reader.Close();
        }
        public void RefreshDataGrid_5(DataGridView dgw)
        {
            dgw.Rows.Clear();

            string querystring = $"SELECT id, dep_cities_id, dest_cities_id, name, phone FROM entries";

            NpgsqlCommand command = new NpgsqlCommand(querystring, dataBase.GetConnection());
            dataBase.OpenConnection();

            NpgsqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                ReadSingleRow_5(dgw, reader);
            }
            reader.Close();
        }
        public void RefreshComboBox_1(System.Windows.Forms.ComboBox a)
        {
            NpgsqlDataAdapter adapter = new NpgsqlDataAdapter();
            DataTable table = new DataTable();
            string querystring = $"select * from busses";
            NpgsqlCommand command = new NpgsqlCommand(querystring, dataBase.GetConnection());
            adapter.SelectCommand = command;
            adapter.Fill(table);

            a.DataSource = table;
            a.DisplayMember = "name";
            a.ValueMember = "id";
        }
        public void RefreshComboBox_2(System.Windows.Forms.ComboBox a)
        {
            NpgsqlDataAdapter adapter = new NpgsqlDataAdapter();
            DataTable table = new DataTable();
            string querystring = $"select * from cities";
            NpgsqlCommand command = new NpgsqlCommand(querystring, dataBase.GetConnection());
            adapter.SelectCommand = command;
            adapter.Fill(table);

            a.DataSource = table;
            a.DisplayMember = "name";
            a.ValueMember = "id";
        }
        public void RefreshComboBox_3(System.Windows.Forms.ComboBox a)
        {
            NpgsqlDataAdapter adapter = new NpgsqlDataAdapter();
            DataTable table = new DataTable();
            string querystring = $"select * from cities";
            NpgsqlCommand command = new NpgsqlCommand(querystring, dataBase.GetConnection());
            adapter.SelectCommand = command;
            adapter.Fill(table);

            a.DataSource = table;
            a.DisplayMember = "name";
            a.ValueMember = "id";
        }
        public void ReadSingleRow_1(DataGridView dgw, IDataRecord reader)
        {
            dgw.Rows.Add(reader.GetInt32(0), reader.GetString(1), reader.GetInt32(2));
        }
        public void ReadSingleRow_2(DataGridView dgw, IDataRecord reader)
        {
            dgw.Rows.Add(reader.GetInt32(0), reader.GetString(1));
        }
        public void ReadSingleRow_3(DataGridView dgw, IDataRecord reader)
        {
            dgw.Rows.Add(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3));
        }
        public void ReadSingleRow_4(DataGridView dgw, IDataRecord reader)
        {
            dgw.Rows.Add(reader.GetInt32(0), reader.GetDateTime(1), reader.GetInt32(2), reader.GetInt32(3), reader.GetInt32(4), reader.GetInt32(5));
        }
        public void ReadSingleRow_5(DataGridView dgw, IDataRecord reader)
        {
            dgw.Rows.Add(reader.GetInt32(0), reader.GetInt32(1), reader.GetInt32(2), reader.GetString(3), reader.GetString(4));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

namespace WindowsFormsApp2
{
    internal class DataBase
    {
        NpgsqlConnection conn = new NpgsqlConnection("Server=localhost;Port=5432;Database=Transportation;User Id=postgres;Password=postgres");

        public void OpenConnection()
        {
            if (conn.State == System.Data.ConnectionState.Closed) 
            { 
                conn.Open(); 
            }
        }
        public void CloseConnection()
        {
            if (conn.State == System.Data.ConnectionState.Open)
            {
                conn.Close();
            }
        }
        public NpgsqlConnection GetConnection()
        {
            return conn;
        }
    }
}

﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp2;

namespace Дипломная_работа_Лоншаков_2023.Classes
{
    internal class CheckRecords
    {
        DataBase dataBase = new DataBase();
        public Boolean checkuser_1(string a, string b, string c)
        {
            NpgsqlDataAdapter adapter = new NpgsqlDataAdapter();
            DataTable table = new DataTable();
            string querystring = $"select id, name, cap from busses where id = '{a}' and name = '{b}' and cap = '{c}'";
            NpgsqlCommand command = new NpgsqlCommand(querystring, dataBase.GetConnection());
            adapter.SelectCommand = command;
            adapter.Fill(table);

            if (table.Rows.Count > 0)
            {
                MessageBox.Show("Запись уже существует!");
                return true;
            }
            else
            {
                return false;
            }
        }
        public Boolean checkuser_2(string a, string b)
        {
            NpgsqlDataAdapter adapter = new NpgsqlDataAdapter();
            DataTable table = new DataTable();
            string querystring = $"select id, name from cities where id = '{a}' and name = '{b}'";
            NpgsqlCommand command = new NpgsqlCommand(querystring, dataBase.GetConnection());
            adapter.SelectCommand = command;
            adapter.Fill(table);

            if (table.Rows.Count > 0)
            {
                MessageBox.Show("Запись уже существует!");
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean checkuser_3(string a, string b, string c)
        {
            NpgsqlDataAdapter adapter = new NpgsqlDataAdapter();
            DataTable table = new DataTable();
            string querystring = $"select  name, login, password from users where name = '{a}' and login = '{b}' and password = '{c}'";
            NpgsqlCommand command = new NpgsqlCommand(querystring, dataBase.GetConnection());
            adapter.SelectCommand = command;
            adapter.Fill(table);

            if (table.Rows.Count > 0)
            {
                MessageBox.Show("Запись уже существует!");
                return true;
            }
            else
            {
                return false;
            }
        }
        public Boolean checkuser_4(string a, string b, string c, string d, string e, string f)
        {
            NpgsqlDataAdapter adapter = new NpgsqlDataAdapter();
            DataTable table = new DataTable();
            string querystring = $"select  id, dep_date, travel_time, dep_cities_id, dest_cities_id, busses_id from flights where id = '{a}' and dep_date = '{b}' and travel_time = '{c}' and dep_cities_id = '{d}' and dest_cities_id = '{e}' and busses_id = '{f}'";
            NpgsqlCommand command = new NpgsqlCommand(querystring, dataBase.GetConnection());
            adapter.SelectCommand = command;
            adapter.Fill(table);

            if (table.Rows.Count > 0)
            {
                MessageBox.Show("Запись уже существует!");
                return true;
            }
            else
            {
                return false;
            }
        }
        public Boolean checkuser_5(string a, string b, string c, string d, string e)
        {
            NpgsqlDataAdapter adapter = new NpgsqlDataAdapter();
            DataTable table = new DataTable();
            string querystring = $"select  id, dep_cities_id, dest_cities_id, name, phone from entries where id = '{a}'and dep_cities_id = '{b}' and dest_cities_id = '{c}' and name = '{d}' and phone = '{e}'";
            NpgsqlCommand command = new NpgsqlCommand(querystring, dataBase.GetConnection());
            adapter.SelectCommand = command;
            adapter.Fill(table);

            if (table.Rows.Count > 0)
            {
                MessageBox.Show("Запись уже существует!");
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

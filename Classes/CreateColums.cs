﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Дипломная_работа_Лоншаков_2023.Classes
{
    internal class CreateColums
    {
        public void CreateColumns_1(DataGridView a)
        {
            a.Columns.Add("id", "ID");
            a.Columns.Add("name", "Название");
            a.Columns.Add("cap", "Вместимость");
        }

        public void CreateColumns_2(DataGridView a)
        {
            a.Columns.Add("id", "ID");
            a.Columns.Add("name", "Название");
        }

        public void CreateColumns_3(DataGridView a)
        {
            a.Columns.Add("id", "ID");
            a.Columns.Add("name", "ФИО");
            a.Columns.Add("login", "Логин");
            a.Columns.Add("password", "Пароль");
        }
        public void CreateColumns_4(DataGridView a)
        {
            a.Columns.Add("id", "ID");
            a.Columns.Add("dep_date", "Дата и время рейса");
            a.Columns.Add("travel_time", "Время пути");
            a.Columns.Add("dep_cities_id", "Населенный пункт выезда");
            a.Columns.Add("dest_cities_id", "Населенный пункт назначения");
            a.Columns.Add("busses_id", "Автобус");
        }
        public void CreateColumns_5(DataGridView a)
        {
            a.Columns.Add("id", "ID");
            a.Columns.Add("dep_cities_id", "Населенный пункт выезда");
            a.Columns.Add("dest_cities_id", "Населенный пункт назначения");
            a.Columns.Add("name", "ФИО");
            a.Columns.Add("phone", "Номер телефона");
        }
    }
}

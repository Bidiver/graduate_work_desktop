﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Дипломная_работа_Лоншаков_2023.Classes;

namespace WindowsFormsApp2
{
    public partial class Form3 : Form
    {
        DataBase dataBase = new DataBase();
        CheckRecords checkRecords = new CheckRecords();
        DeleteRow deleteRow = new DeleteRow();
        RefreshDate refreshDate = new RefreshDate();
        CreateColums createColums = new CreateColums();

        int slectedRow;
        public Form3()
        {
            InitializeComponent();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text != "")
            {
                if (checkRecords.checkuser_1(textBox1.Text, textBox2.Text, textBox3.Text))
                {
                    return;
                }
                try
                {
                    string querystring = $"insert into busses(id, name, cap) values('{textBox1.Text}', '{textBox2.Text}', '{textBox3.Text}')";
                    NpgsqlCommand command = new NpgsqlCommand(querystring, dataBase.GetConnection());
                    dataBase.OpenConnection();

                    if (command.ExecuteNonQuery() == 1)
                    {
                        MessageBox.Show("Запись успешно создана!", "Успех!");
                        refreshDate.RefreshDataGrid_1(dataGridView1);
                        refreshDate.RefreshComboBox_1(comboBox1);
                    }
                    else
                    {
                        MessageBox.Show("Запись не создана!");
                    }
                }
                catch
                {
                    MessageBox.Show("Непредвиденная ошибка!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                dataBase.CloseConnection();
            }
            else
            {
                MessageBox.Show("Заполните все поля!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            int index = dataGridView1.CurrentRow.Index;
            string num_book = Convert.ToString(dataGridView1[0, index].Value);
            deleteRow.deleteRow_1(num_book, dataGridView1, comboBox1);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text != "")
            {
                string cmd_text = "UPDATE busses SET name = '" + textBox2.Text + "', " +
                "cap = '" + textBox3.Text + "' WHERE id = '" + textBox1.Text + "'";

                NpgsqlCommand command = new NpgsqlCommand(cmd_text, dataBase.GetConnection());

                dataBase.OpenConnection();
                command.ExecuteNonQuery();
                dataBase.CloseConnection();

                refreshDate.RefreshDataGrid_1(dataGridView1);
                refreshDate.RefreshComboBox_1(comboBox1);
            }
            else
            {
                MessageBox.Show("Заполните все поля!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            refreshDate.RefreshDataGrid_1(dataGridView1);
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            slectedRow = e.RowIndex;
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = dataGridView1.Rows[slectedRow];
                textBox1.Text = row.Cells[0].Value?.ToString();
                textBox2.Text = row.Cells[1].Value?.ToString();
                textBox3.Text = row.Cells[2].Value?.ToString();
            }
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            createColums.CreateColumns_1(dataGridView1);
            createColums.CreateColumns_2(dataGridView2);
            createColums.CreateColumns_3(dataGridView3);
            createColums.CreateColumns_4(dataGridView4);
            createColums.CreateColumns_5(dataGridView5);
            refreshDate.RefreshDataGrid_1(dataGridView1);
            refreshDate.RefreshDataGrid_2(dataGridView2);
            refreshDate.RefreshDataGrid_3(dataGridView3);
            refreshDate.RefreshDataGrid_4(dataGridView4);
            refreshDate.RefreshDataGrid_5(dataGridView5);
            refreshDate.RefreshComboBox_1(comboBox1);
            refreshDate.RefreshComboBox_2(comboBox4);
            refreshDate.RefreshComboBox_3(comboBox5);
            refreshDate.RefreshComboBox_2(comboBox2);
            refreshDate.RefreshComboBox_3(comboBox3);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (textBox6.Text != "" && textBox5.Text != "")
            {
                if (checkRecords.checkuser_2(textBox6.Text, textBox5.Text))
                {
                    return;
                }
                try
                {
                    string querystring = $"insert into cities(id, name) values('{textBox6.Text}', '{textBox5.Text}')";
                    NpgsqlCommand command = new NpgsqlCommand(querystring, dataBase.GetConnection());
                    dataBase.OpenConnection();

                    if (command.ExecuteNonQuery() == 1)
                    {
                        MessageBox.Show("Запись успешно создана!", "Успех!");
                        refreshDate.RefreshDataGrid_2(dataGridView2);
                        refreshDate.RefreshComboBox_2(comboBox2);
                        refreshDate.RefreshComboBox_3(comboBox3);
                    }
                    else
                    {
                        MessageBox.Show("Запись не создана!");
                    }
                }
                catch
                {
                    MessageBox.Show("Непредвиденная ошибка!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                dataBase.CloseConnection();
            }
            else
            {
                MessageBox.Show("Заполните все поля!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (textBox6.Text != "" && textBox5.Text != "")
            {
                string cmd_text = "UPDATE cities SET name = '" + textBox5.Text + "' WHERE id = '" + textBox6.Text + "'";

                NpgsqlCommand command = new NpgsqlCommand(cmd_text, dataBase.GetConnection());

                dataBase.OpenConnection();
                command.ExecuteNonQuery();
                dataBase.CloseConnection();

                refreshDate.RefreshDataGrid_2(dataGridView2);
                refreshDate.RefreshComboBox_2(comboBox2);
                refreshDate.RefreshComboBox_3(comboBox3);
            }
            else
            {
                MessageBox.Show("Заполните все поля!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            int index = dataGridView2.CurrentRow.Index;
            string num_book = Convert.ToString(dataGridView2[0, index].Value);
            deleteRow.deleteRow_2(num_book, dataGridView2, comboBox2, comboBox3);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            refreshDate.RefreshDataGrid_2(dataGridView2);
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            slectedRow = e.RowIndex;
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = dataGridView2.Rows[slectedRow];

                textBox6.Text = row.Cells[0].Value?.ToString();
                textBox5.Text = row.Cells[1].Value?.ToString();
            }
        }

        private void dataGridView3_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            slectedRow = e.RowIndex;
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = dataGridView3.Rows[slectedRow];

                textBox9.Text = row.Cells[0].Value?.ToString();
                textBox8.Text = row.Cells[1].Value?.ToString();
                textBox7.Text = row.Cells[2].Value?.ToString();
                textBox4.Text = row.Cells[3].Value?.ToString();
            }
        }

        private void button15_Click(object sender, EventArgs e)
        {
            if (textBox8.Text != "" && textBox7.Text != "" && textBox4.Text != "")
            {
                if (checkRecords.checkuser_3(textBox8.Text, textBox7.Text, textBox4.Text))
                {
                    return;
                }
                try
                {
                    string querystring = $"insert into users(id, name, login, password) values('{textBox9.Text}', '{textBox8.Text}', '{textBox7.Text}', '{textBox4.Text}')";
                    NpgsqlCommand command = new NpgsqlCommand(querystring, dataBase.GetConnection());
                    dataBase.OpenConnection();

                    if (command.ExecuteNonQuery() == 1)
                    {
                        MessageBox.Show("Запись успешно создана!", "Успех!");
                        refreshDate.RefreshDataGrid_2(dataGridView2);
                    }
                    else
                    {
                        MessageBox.Show("Запись не создана!");
                    }
                }
                catch
                {
                    MessageBox.Show("Непредвиденная ошибка!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                dataBase.CloseConnection();
            }
            else
            {
                MessageBox.Show("Заполните все поля!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void button14_Click(object sender, EventArgs e)
        {
            if (textBox8.Text != "" && textBox7.Text != "" && textBox4.Text != "")
            {
                string cmd_text = "UPDATE users SET login = '" + textBox7.Text + "', " +
                               "password = '" + textBox4.Text + "', " + "name = '" + textBox8.Text + "' WHERE id = '" + textBox9.Text + "'";

                NpgsqlCommand command = new NpgsqlCommand(cmd_text, dataBase.GetConnection());

                dataBase.OpenConnection();
                command.ExecuteNonQuery();
                dataBase.CloseConnection();

                refreshDate.RefreshDataGrid_3(dataGridView3);
            }
            else
            {
                MessageBox.Show("Заполните все поля!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void button13_Click(object sender, EventArgs e)
        {
            int index = dataGridView3.CurrentRow.Index;
            string num_book = Convert.ToString(dataGridView3[0, index].Value);
            deleteRow.deleteRow_3(num_book, dataGridView3);
        }

        private void button12_Click(object sender, EventArgs e)
        {
            refreshDate.RefreshDataGrid_3(dataGridView3);
        }

        private void button17_Click(object sender, EventArgs e)
        {
            refreshDate.RefreshDataGrid_4(dataGridView4);
        }

        private void dataGridView4_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            slectedRow = e.RowIndex;
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = dataGridView4.Rows[slectedRow];

                textBox10.Text = row.Cells[0].Value?.ToString();
                textBox13.Text = row.Cells[1].Value?.ToString();
                textBox12.Text = row.Cells[2].Value?.ToString();
                comboBox3.SelectedValue = row.Cells[3].Value.ToString();
                comboBox2.SelectedValue = row.Cells[4].Value.ToString();
                comboBox1.SelectedValue = row.Cells[5].Value.ToString();
            }
        }

        private void button20_Click(object sender, EventArgs e)
        {
            if(textBox10.Text != "" && textBox13.Text != "" && textBox12.Text != "")
            {
                if (checkRecords.checkuser_4(textBox10.Text, textBox13.Text, textBox12.Text, Convert.ToString(comboBox3.SelectedValue), Convert.ToString(comboBox2.SelectedValue), Convert.ToString(comboBox1.SelectedValue)))
                {
                    return;
                }
                try
                {
                    string querystring = $"insert into flights (id, dep_date, travel_time, dep_cities_id, dest_cities_id, busses_id) values('{textBox10.Text}', '{textBox13.Text}', '{textBox12.Text}', '{Convert.ToString(comboBox3.SelectedValue)}', '{Convert.ToString(comboBox2.SelectedValue)}', '{Convert.ToString(comboBox1.SelectedValue)}')";
                    NpgsqlCommand command = new NpgsqlCommand(querystring, dataBase.GetConnection());
                    dataBase.OpenConnection();

                    if (command.ExecuteNonQuery() == 1)
                    {
                        MessageBox.Show("Запись успешно создана!", "Успех!");
                        refreshDate.RefreshDataGrid_4(dataGridView4);
                    }
                    else
                    {
                        MessageBox.Show("Запись не создана!");
                    }
                }
                catch
                {
                    MessageBox.Show("Непредвиденная ошибка!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                dataBase.CloseConnection();
            }
            else
            {
                MessageBox.Show("Заполните все поля!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void button19_Click(object sender, EventArgs e)
        {
            if (textBox13.Text != "" && textBox12.Text != "" && textBox10.Text != "")
            {
                string cmd_text = "UPDATE flights SET dep_date = '" + textBox13.Text + "', " +
               "travel_time = '" + textBox12.Text + "', " + "dep_cities_id = '" + Convert.ToString(comboBox3.SelectedValue) + "', " + "dest_cities_id = '" + Convert.ToString(comboBox2.SelectedValue) + "', " + "busses_id = '" + Convert.ToString(comboBox1.SelectedValue) + "' WHERE id = '" + textBox10.Text + "'";

                NpgsqlCommand command = new NpgsqlCommand(cmd_text, dataBase.GetConnection());

                dataBase.OpenConnection();
                command.ExecuteNonQuery();
                dataBase.CloseConnection();

                refreshDate.RefreshDataGrid_4(dataGridView4);
            }
            else
            {
                MessageBox.Show("Заполните все поля!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            int index = dataGridView4.CurrentRow.Index;
            string num_book = Convert.ToString(dataGridView4[0, index].Value);
            deleteRow.deleteRow_4(num_book, dataGridView4);
        }

        private void Form3_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button16_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void dataGridView5_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            slectedRow = e.RowIndex;
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = dataGridView5.Rows[slectedRow];

                textBox11.Text = row.Cells[0].Value?.ToString();
                comboBox4.SelectedValue = row.Cells[1].Value.ToString();
                comboBox5.SelectedValue = row.Cells[2].Value.ToString();
                textBox15.Text = row.Cells[3].Value?.ToString();
                textBox14.Text = row.Cells[4].Value?.ToString();
            }
        }

        private void button25_Click(object sender, EventArgs e)
        {
            if (textBox11.Text != "" && textBox15.Text != "" && textBox14.Text != "")
            {
                if (checkRecords.checkuser_5(textBox11.Text, Convert.ToString(comboBox4.SelectedValue), Convert.ToString(comboBox5.SelectedValue), textBox15.Text, textBox14.Text))
                {
                    return;
                }
                try
                {
                    string querystring = $"insert into entries (id, dep_cities_id, dest_cities_id, name, phone) values('{textBox11.Text}', '{Convert.ToString(comboBox4.SelectedValue)}', '{Convert.ToString(comboBox5.SelectedValue)}', '{textBox15.Text}', '{textBox14.Text}')";
                    NpgsqlCommand command = new NpgsqlCommand(querystring, dataBase.GetConnection());
                    dataBase.OpenConnection();

                    if (command.ExecuteNonQuery() == 1)
                    {
                        MessageBox.Show("Запись успешно создана!", "Успех!");
                        refreshDate.RefreshDataGrid_5(dataGridView5);
                    }
                    else
                    {
                        MessageBox.Show("Запись не создана!");
                    }
                }
                catch
                {
                    MessageBox.Show("Непредвиденная ошибка!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                dataBase.CloseConnection();
            }
            else
            {
                MessageBox.Show("Заполните все поля!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void button24_Click(object sender, EventArgs e)
        {
            if (textBox11.Text != "" && textBox15.Text != "" && textBox14.Text != "")
            {
                string cmd_text = "UPDATE entries SET dep_cities_id = '" + Convert.ToString(comboBox4.SelectedValue) + "', " + "dest_cities_id = '" + Convert.ToString(comboBox5.SelectedValue) + "', " + "name = '" + textBox15.Text + "', " + "phone = '" + textBox14.Text + "' WHERE id = '" + textBox11.Text + "'";

                NpgsqlCommand command = new NpgsqlCommand(cmd_text, dataBase.GetConnection());

                dataBase.OpenConnection();
                command.ExecuteNonQuery();
                dataBase.CloseConnection();

                refreshDate.RefreshDataGrid_5(dataGridView5);
            }
            else
            {
                MessageBox.Show("Заполните все поля!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void button23_Click(object sender, EventArgs e)
        {
            int index = dataGridView5.CurrentRow.Index;
            string num_book = Convert.ToString(dataGridView5[0, index].Value);
            deleteRow.deleteRow_5(num_book, dataGridView5);
        }

        private void button22_Click(object sender, EventArgs e)
        {
            refreshDate.RefreshDataGrid_5(dataGridView5);
        }

        private void button21_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}